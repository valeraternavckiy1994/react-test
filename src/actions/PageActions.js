export function searchAction(currentWeather){
    return {
        type: "CURRENT_WEATHER",
        payload: currentWeather 
    }
}

export function addHistory(currentWeather){
    return {
        type: "ADD_HISTORY",
        payload: currentWeather
    }
}

