let initState = [];

export default function addHistory(state = initState, {payload,type}){
  if(type == 'ADD_HISTORY'){
      return [
              ...state,
              {
                id:payload.id,
                name: payload.name
              }
          ]
      
  }else{
      return state;
  }
}
