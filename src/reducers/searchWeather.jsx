let initState = {};

export default function searchWeather(state = initState, action){
  if(action.type == 'CURRENT_WEATHER'){
      return {
          ...action.payload
      }
  }else{
      return state;
  }
}
