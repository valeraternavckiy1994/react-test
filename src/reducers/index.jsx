import { combineReducers } from 'redux';
import searchWeather from './searchWeather';
import addHistory from './addHistory';


export default combineReducers(
    {
        searchWeather,
        addHistory
    }
);