function getApiUrl(protocol, url){
    return `${protocol}://${url}/`;
}

export default getApiUrl;