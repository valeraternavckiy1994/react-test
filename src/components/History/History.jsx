import React, { Component } from 'react';
import {connect} from 'react-redux';

let History = props => {    
    return (
        <div className='history__block'> 
            <h3>Search Histor</h3>
            <ol className='history__list'>
                {props.histories.map((history, index) => <li key={index}> { history.name } </li>)}
            </ol>
        </div>
    );
}

export default connect(state => ({histories:state.addHistory}))(History);