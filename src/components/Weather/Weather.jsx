import React, { Component } from 'react';
import { connect } from 'react-redux'
import setRestInterface from '../../hoc/withRest';
import Search from '../Search/Search';
import History from '../History/History';
import WeatherList from '../WeatherList/WeatherList';
import getApiUrl from '../../constants/getApiUrl';
import  * as pageActions  from '../../actions/PageActions';
import './weather.css'

const url = getApiUrl('http', 'api.openweathermap.org/data/2.5/weather'),
      apiKey = "2a74553471abac4fc7c6b0a9bd648794";

class Weather extends Component {
    state = {}

    constructor(props){
        super(props);
    }   

    onGetWetherData = (params) => {
        this.props.get(params)
            .then(this.props.mapDispatchToProps, this.props.mapDispatchToProps);
    }

    render(){
        return (
            <article className='weather'> 
                <div className="weather__search">
                    <Search search={this.onGetWetherData} 
                            defaultValue={'Kiev'}
                            getQueryParams={(element) => ({q:element.value? element.value: 'Kiev' ,APPID:apiKey})}/>
                </div>
                <div className="weather__list">
                     <WeatherList />
                </div>
                <div className="weather__search-history">
                    <History />
                </div>
            </article>
        );
    }
}

const HOC = setRestInterface(Weather, url);

export default connect(
    state =>({}), 
    dispatch => {
        return {
            mapDispatchToProps: ({data}) => {
                dispatch(pageActions.searchAction(data || {}));
                
                if(data)
                    dispatch(pageActions.addHistory(data));
            }
        }
    }
)(HOC);