import React, { Component } from 'react';
import debounce from '../../common/debounce'
import './search.css';


const debounceInterval = 1000;

class History extends Component{
    constructor(props){
        super(props);
        this.onSearch = debounce(this.onSearch, debounceInterval);
    }

    onSearch = () => { 
        let params = this.props.getQueryParams(this.refs.searchField);
        this.props.search(params);
        // fevent? event.preventDefault(): 
    }

    componentWillMount(){
        this.onSearch();
    }
    
    render(){
        return (
            <div className='search-block'> 
                <label htmlFor="search">Search</label>
                <input type="search" 
                        id="search" 
                        ref="searchField"
                        className="search-block__field" />
                <button onClick={this.onSearch}> Search</button>
            </div>
        );
    } 
}

export default History;