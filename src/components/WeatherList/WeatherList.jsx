import React, { Component } from 'react';
import {connect} from 'react-redux';
import './weather-list.css';

function getDate(date){
    let correctDate = new Date(date * 1000);
    return `${correctDate.getFullYear()}-${correctDate.getMonth() + 1}-${correctDate.getDate()}`;
}

let WeatherList = ({weatherData}) => { 
        return (
            <div>
                <ul className={`weather-list__inner ${(weatherData.name)? '':'weather-list__inner--hidden'}`} >
                    <li className="weather-list__city">
                        <strong>City:</strong> {weatherData.name}
                    </li>
                    <li className='weather-list__date'>
                        <strong>Date: </strong> {`${weatherData.dt ? getDate(weatherData.dt) : ''}`}
                    </li> 
                    <li> 
                        <strong>Temp: </strong> {weatherData.main? (parseInt(weatherData.main.temp - 273.15)) : null} °C
                    </li>
                </ul>
                <div className={`weather-list__error-text ${(weatherData.name)? 'weather-list__error-text--hidden': ''}`}>City not found</div>
            </div>
        );
}   

function mapStateToProps({searchWeather}){
    return {
        weatherData: searchWeather
    };
}

export default connect(mapStateToProps)(WeatherList);