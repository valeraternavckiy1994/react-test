import React from 'react';
import axios from 'axios';


function setRestInterface(Component, apiUrl) {
    class SetRestInterface extends React.Component{
        constructor(props){
            super(props);
        }

        get = (params) => {
           return axios.get(apiUrl, {params})
        }

        render(){
            return <Component { ...this.props } 
                              get={ this.get }/>
        }
    }

    SetRestInterface.displayName = `SetRestInterface(${Component.displayName || Component.name || 'Component'})`;
    return SetRestInterface;
 }

export default setRestInterface;