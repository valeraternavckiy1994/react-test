import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import multi from 'redux-multi'
import reducer from './reducers'
import * as serviceWorker from './serviceWorker';


applyMiddleware(multi)(createStore);

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}> 
        <App />
    </Provider>,
    document.getElementById('root'));


serviceWorker.unregister();
